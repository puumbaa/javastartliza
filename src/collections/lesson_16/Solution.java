package collections.lesson_16;

import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(Resources.values()));


    }
}
class CoffeeMachine{
    // todo ... поля B = milk
    Resources[] resources = Resources.values();

    public void setMilk(int milk){
        for (Resources resource : resources) {
            if (resource.equals(Resources.B)){
                resource.setLevel(resource.getLevel() - milk);
            }
        }
    }


}
enum Resources {
    A(10), B(100), C(200);

    int level;

    @Override
    public String toString() {
        return "Resources{" +
                "a=" + level +
                '}';
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    Resources(int level) {
        this.level = level;
    }
}
