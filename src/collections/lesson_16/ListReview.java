package collections.lesson_16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListReview {
    public static void main(String[] args) {
        //        Arrays.asList(5, 10, 15, 20, 3, 4, 9);
        List<Integer> list = new ArrayList<>(Arrays.asList(5, 10, 15, 20, 3, 4, 9));
        Integer[] array = new Integer[list.size()];
        list.toArray(array);
        System.out.println(Arrays.toString(array));
        System.out.println("индекс элемента 20 = " + list.indexOf(20));
        list.remove((Integer) 20);
        System.out.println(list);

//        System.out.println(list.contains(5));
//        System.out.println(list);
    }
}
