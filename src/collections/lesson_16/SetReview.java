package collections.lesson_16;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetReview {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>(Arrays.asList(5, 10, 15, 10, 3, 4, 10));
        set.add(1);
        set.add(2);
//        System.out.println(set.contains(10));

        int[] numbers = {5, 10, 15, 3, 4};
        Set<Integer> hashSet = new HashSet<>();
        for (int number : numbers) {
            hashSet.add(number);
        }
        System.out.println(numbers.length == hashSet.size());



/*        for (Integer integer : set) {
            System.out.println(integer);
        }*/
    }
}
