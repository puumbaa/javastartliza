package collections.lesson_16;

import java.util.*;

// Queue - однонаправленная очередь
// poll - возвращает первый элемент и удаляет его
// element - возвращает первый элемент но не удаляет его
public class QueueReview {
    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();
        queue.add(5);
        queue.add(1);
        queue.add(3);
        queue.add(0);
        System.out.println(queue);
        System.out.println(queue.poll());
        System.out.println(queue.poll());
//        queue.add(40);
/*        System.out.println("до poll()" + queue);
        System.out.println("queue.poll() >> " + queue.poll());
        System.out.println("queue.element() >> " + queue.element());
        System.out.println("после poll()" + queue);
        System.out.println("offer() >> " + queue.offer(50));
        System.out.println("после offer " + queue);
        System.out.println("remove() >> " + queue.remove());
        System.out.println("после remove " + queue);
        System.out.println("peek() >> " + queue.peek());
        System.out.println("после peek " + queue);*/
/*
        Deque<String> deque = new ArrayDeque<>();
        deque.addFirst("hello1");
        deque.addFirst("hello2");
        deque.addFirst("hello3");
        deque.addLast("hello4");
        System.out.println(deque);*/


    }
}
