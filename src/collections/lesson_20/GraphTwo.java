package collections.lesson_20;

import java.util.*;


public class GraphTwo {
//    private boolean[][] adjacencyMatrix; // матрица смежности
//    private static List<Integer>[] adjacencyList; // лист смежности
    private static int[][] adjacencyMatrix;


    public static class VertexPathComparator implements Comparator<Integer>{

        private int startVertex;

        public VertexPathComparator(int startVertex) {
            this.startVertex = startVertex;
        }

        @Override// v1 = 2 |  v2 = 1

        public int compare(Integer vertexNumber1, Integer vertexNumber2) {
            return adjacencyMatrix[startVertex][vertexNumber1] - adjacencyMatrix[startVertex][vertexNumber2];
        }
    }



    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        adjacencyMatrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                adjacencyMatrix[i][j] = sc.nextInt();
            }
        }
        // с какой вершины искать кратчайшие пути от 0 до n-1
        int startVertex = sc.nextInt();

        dijkstra(adjacencyMatrix, startVertex);
    }

    private static void dijkstra(int[][] adjacencyMatrix, int start) {
        Map<Integer, Integer> vertexPathCostMap = new HashMap<>();
        boolean[] visited = new boolean[adjacencyMatrix.length];

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            vertexPathCostMap.put(i, Integer.MAX_VALUE);
        }
        vertexPathCostMap.put(start, 0);



        processVertex(adjacencyMatrix, start, vertexPathCostMap, visited);
        System.out.println(vertexPathCostMap);


    }

    private static void processVertex(int[][] adjacencyMatrix, int start, Map<Integer, Integer> vertexPathCostMap, boolean[] visited) {
        Queue<Integer> adjacencyVertexes = new PriorityQueue<>(new VertexPathComparator(start));
        Queue<Integer> queueToProcess = new ArrayDeque<>();

        // заполняем очередь соседних вершин, упорядоченных в порядке расстояния до них
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[start][i] > 0 && !visited[i]) {
                adjacencyVertexes.add(i);
            }
        }

        int size = adjacencyVertexes.size();
        // перебираем каждого соседа, начиная с ближайшего и обновляем мапу кратчайших путей
        for (int i = 0; i < size; i++) {
            Integer neighbour = adjacencyVertexes.poll();
            queueToProcess.add(neighbour);
            int newPath = adjacencyMatrix[start][neighbour] + vertexPathCostMap.get(start);
            int oldPath = vertexPathCostMap.get(neighbour);

            if (newPath < oldPath) {
                vertexPathCostMap.put(neighbour, newPath);
            }
        }
        visited[start] = true;

        while (!queueToProcess.isEmpty()){
            Integer neighbour = queueToProcess.poll();
            processVertex(adjacencyMatrix,neighbour,vertexPathCostMap,visited);
        }
    }



}
