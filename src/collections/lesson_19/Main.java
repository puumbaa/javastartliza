package collections.lesson_19;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//        getNameCatMap(Arrays.asList(....))
        List<Integer> list = new ArrayList<>();

        Cat parent = new Cat("p", 12, null, null, null);
        Cat c1 = new Cat("c1", 12, parent, null, null);
        Cat c2 = new Cat("c2", 12, parent, null, null);

        List<Cat> cats = new ArrayList<>();
        cats.add(c1);
        cats.add(c2);
        System.out.println(getCatChildrenMap(cats));
        Collections.sort(cats, new Cat.CatComparator());


        int[] arr = {1,2,34};
        sumWithArray(arr);

        sumWithVarargs(1,2,34);


    }


    public static Map<String, Cat> getNameCatMap(List<Cat> cats) { // cats = [c1,c2,c3]

        Map<String, Cat> nameCatMap = new HashMap<>(); // [] - > ['cat1' - c1, 'cat2' - c2, 'cat3' - c3]

        for (Cat cat : cats) {
            nameCatMap.put(cat.name, cat);
        }
        return nameCatMap;
    }

    // ключ - возраст
    // значение - список кошек с таким возрастом
    // [3 - [c1,c2,c3,c4]; ]
    public static Map<Integer, List<Cat>> getAgeCatsMap(List<Cat> cats) {

        Map<Integer, List<Cat>> ageCatsMap = new HashMap<>(); // [null - null;]

        for (Cat cat : cats) {
/*            if (ageCatsMap.get(cat.age) == null){ // Ситация когда еще не было никакого списка кошек с таким возрастом, и наша кошка была первая в этом списке
                List<Cat> list = new ArrayList<>();
            }else { // Случай когда мы добавляем текущую кошку к уже существующему списку кошек
                List<Cat> list = ageCatsMap.get(cat.age);
            }*/

            List<Cat> list = ageCatsMap.get(cat.age) == null ? new ArrayList<>() : ageCatsMap.get(cat.age);
            list.add(cat);
            ageCatsMap.put(cat.age, list);
        }
        return ageCatsMap;
    }


    public static Map<Cat, List<Cat>> getCatChildrenMap(List<Cat> cats) {
        Map<Cat, List<Cat>> catChildrenMap = new HashMap<>();

        for (Cat cat : cats) {
            Cat father = cat.fatherCat;
            Cat mother = cat.motherCat;

            addChildToParent(catChildrenMap, cat, father);
            addChildToParent(catChildrenMap, cat, mother);
        }
        return catChildrenMap;
    }


/*    public static int sum(int a, int b){
        return a + b;
    }
    public static int sum(int a, int b, int c){
        return a + b + c;
    }*/
    public static int sumWithArray(int[] numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }
    // sum(1,23,34,5,6);
    // sum(1,2,3);
    public static int sumWithVarargs(int... numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }


    // добавить ребенка родителю
    private static void addChildToParent(Map<Cat, List<Cat>> catChildrenMap, Cat cat, Cat parent) {
        if (parent == null) return;
        List<Cat> children;
        if (catChildrenMap.containsKey(parent)) {
            children = catChildrenMap.get(parent);
        } else {
            children = new ArrayList<>();
        }
        children.add(cat);
        catChildrenMap.put(parent, children);
    }
}
