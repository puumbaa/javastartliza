package collections.lesson_19;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Cat implements Iterable<Cat>{
    Cat[] data;
    String name;
    int age;
    Cat fatherCat;
    Cat motherCat;
    String color;

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", fatherCat=" + fatherCat +
                '}';
    }

    public Cat(String name, int age, Cat fatherCat, Cat motherCat, String color) {
/*        if (fatherCat == null || motherCat == null) {
            throw new IllegalArgumentException("cat must have parents!");
        }*/
        this.name = name;
        this.age = age;
        this.fatherCat = fatherCat;
        this.motherCat = motherCat;
        this.color = color;
    }

    @Override
    public Iterator<Cat> iterator() {
        return new CatIterator();
    }

    public class CatIterator implements Iterator<Cat>{

        int i = 0;

        @Override
        public boolean hasNext() {
            return i < data.length;
        }

        @Override
        public Cat next() {
            return data[i];
        }
    }


    public static class CatComparator implements Comparator<Cat> {

        @Override
        public int compare(Cat o1, Cat o2) {
            return o1.name.compareTo(o2.name);
        }
    }

}
