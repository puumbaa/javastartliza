package collections.lesson_18;

import java.util.InputMismatchException;
import java.util.Scanner;
// javac Solution.java - команда для компиляция (исходный код => байт код). Это для JVM
// java Solution - процесс скармливания скомпированного байткода JVM - запуск программы
public class Solution {

    // 5! = 1 * 2 * 3 .. * 5
    public static int fact(int n){
        if (n == 1) return 1;
        return n * fact(n - 1);
    }

    public static void main(String[] args) {
        int result = fact(5);
        System.out.println(result);

//        int n = 5;
//        switch (n){
//            case 1:
//                System.out.println("n = 1");
//                break;
//            case 5:
//                System.out.println("n = 5");
//                break;
//            case 6:
//                System.out.println("n = 6");
//                break;
//            default:
//                System.out.println("недоступная команда");
//        }
//        System.out.println("свитч кейс завершился тут");
        Scanner sc = new Scanner(System.in);
//        int x = sc.nextInt();
//        int y = sc.nextInt();
//        int[] arr = {1,2,3};
//        try {
//            System.out.println("результат = " + x/y);
//            System.out.println("элемент массива под индексом " + x + " = " + arr[x]);
//        }catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
//            System.out.println("выход за границы массива");
//        } catch (ArithmeticException e){
//            e.printStackTrace();
//            System.out.println("нельзя делить на 0");
//        }finally {
//            System.out.println("я всегда выполняюсь");
//        }


//        try {
//            dangerMethod();
//        } catch (NoResourcesException e) {
//            throw new IllegalStateException();
//        }
//        System.out.println("я тут");
        getNumber(0);

        int number = -1;
        do {
            try {
                number = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("вы ввели не числовое значение");
            }
            sc.nextLine();
        } while (number == -1);


        if (number == 1) {
            System.out.println(1);
        }
        if (number == 2) {
            System.out.println(2);
        }
        if (number == 3) {
            System.out.println(3);
        }


    }

    public static void getNumber(int x) {
        int y = 4;
        if (x == 0) {
            System.out.println(5);
        } else {
            throw new IllegalArgumentException("плохой параметр");
        }
    }

    public static void dangerMethod() throws NoResourcesException {
        Scanner sc = new Scanner(System.in);


        int resource = 10;
        int coffeeRequiredResource = sc.nextInt(); // сколько требуется ресурса для приготовления кофе

        if (resource < coffeeRequiredResource) {
            throw new NoResourcesException();
        }
    }
}
