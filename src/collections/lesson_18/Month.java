package collections.lesson_18;

public enum Month {
    JUNE(1), JULE(2), AUGUST(4);
    private int i;

    Month(int i) {
        this.i = i;
    }

/*
    public Month() {
    }
*/

    public int getI() {
        return i;
    }

    void setI(int i) {
        this.i = i;
    }
}
