package collections.lesson_18;

public class NoResourcesException extends Exception{

    public NoResourcesException(String message) {
        super(message);
    }

    public NoResourcesException() {
    }
}
