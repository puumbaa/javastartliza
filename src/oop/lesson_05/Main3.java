package oop.lesson_05;

import java.util.Scanner;

public class Main3 {

    public static void main(String[] args){
        //    int[] arr = new int[5]; - одномерный массив, хранит в себе int-ы
        Scanner sc = new Scanner(System.in);
        System.out.println("введите кол-во массивов: ");
        int countOfArrays = sc.nextInt();

        int[][] arr = new int[countOfArrays][countOfArrays];
//        String s = null; s по умолчанию хранит в себе значение null

        for (int i = 0; i < countOfArrays; i++) {
            System.out.println("Введите значение массива №" + (i+1));
            for (int j = 0; j < countOfArrays; j++) {
                arr[i][j] = sc.nextInt();
            }
        }

        /*for(int[] x: arr){
            System.out.println(Arrays.toString(x));
        }*/

        for (int i = 0; i < countOfArrays; i++) {
            for (int j = 0; j < countOfArrays; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }

    }
}
