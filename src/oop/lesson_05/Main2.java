package oop.lesson_05;

import java.util.Arrays;

public class Main2 {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        String[] strArray = {"asd", "abc","aaa"};
       /*
       *  1
       *  2
       * 3
       * 4
       * -> 5
       */
        int y = arr[0];
        y++;
        System.out.println(arr[0]);
/*
        Две эквивалентные записи
        int i =0;
        while (i < arr.length){
            arr[i]++;
            i++;
        }
        for (int j = 0; j < arr.length; j++) {
            arr[i]++;
        }*/

        for (int x: arr) {
            x++;
        }
/*        for (String s: strArray){
            System.out.println(s);
        }*/
        System.out.println(Arrays.toString(arr));
    }
}

