package oop.lesson_05;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    // 2, 3, 0, | 4, 0, 5, 0 - > 0, 0, 2, 3, 4, 5

    // 0 1 2 3 0 4
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int size = sc.nextInt();
        int[] arr = new int[size];
        System.out.println("Введите значения массива: ");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }
        int left = 0, right = size - 1;
        while (left < right) {
            while (arr[left] == 0) { // поменять
                left++;
            }
            while (arr[right] != 0) { // поменять
                right--;
            }
            if (arr[left] != 0 && arr[right] == 0 && left <= right) {
                int t = arr[left];
                arr[left] = arr[right];
                arr[right] = t;
                left++;
                right--;
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}

