package oop.lesson_05.hw;

import java.util.Scanner;

public class HomeWork5 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число Х: ");
        int x = sc.nextInt();
        boolean isFoundX = false; // Мы нашли уже X ?  изначально - ответ `нет`


        for (int i = 0; i < array.length; i++) {
            if (array[i] == x) {
                isFoundX = true;
                System.out.println(i);
            }
        }
        if (!isFoundX){
            System.out.println("Числа равного " + x + " не существует");
        }
    }

}


