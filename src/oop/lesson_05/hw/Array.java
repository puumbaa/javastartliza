package oop.lesson_05.hw;

import java.util.Scanner;


public class Array {
    // Двумерные массивы
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите кол-во массивов: ");
        int countOfArrays = sc.nextInt();

        int[][] arr = new int[countOfArrays][countOfArrays];

        for (int i = 0; i < countOfArrays; i++) {
            System.out.println("Введите значение массива №" + (i + 1));
            for (int j = 0; j < countOfArrays; j++) {
                arr[i][j] = sc.nextInt();
            }
        }

        for (int i = 0; i < countOfArrays; i++) {
            //
            for (int j = 0; j < countOfArrays; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }

        int sum = 0;
        for (int i = 0; i < countOfArrays; i++) {
            for (int j = 0; j < countOfArrays; j++) {
                sum = sum + arr[i][j];
            }
        }
        System.out.println("Сумма всех элементов массивов = " + sum);
        System.out.println("Количество всех элементов массивов = " + countOfArrays * countOfArrays);

        System.out.println("Введите число Х");
        int x = sc.nextInt();

        for (int i = 0; i < countOfArrays; i++) {
            for (int j = 0; j < countOfArrays; j++) {
                if (arr[i][j] == x) {
                    System.out.println(true);
                } else {
                    System.out.println(false);
                }
            }
        }

    }
}