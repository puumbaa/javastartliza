package oop.lesson_09;

public class Solution {

    static boolean isBelongs(int a, int b, int c) {
        /*
        if (c > a && c < b) { - это плохо
            return true;
        }else{
            return false;
        }*/
        return c > a && c < b;
    }
    // 1243123

    static int countOfDigits(int number) {
        // 12314 % 10 = 4
        // 12 % 10 - остаток от деления  12 на 10 = 2
        // 123 / 10 - деление целочисленное 123 на 10 = 12

        int count = 0;
        while (number > 0) {
            count++;
            number /= 10;// то же самое что и number = number / 10;
        }
        return count;
    }

    public static void main(String[] args) {
//        System.out.println(countOfDigits(12314));
//        System.out.println(isBelongs(2,5,4) ? "принадлежит" : "не принадлежит");

        Person p = new Person("Эльнур", "Сардаров", 19);
        Person.bla = "p";

        Person p2 = new Person("Лиза");
        Person.bla = "p2";

        System.out.println(Person.sum(4, 5));

//        System.out.println(p.bla); // p2

//        System.out.println(p.firstName);
        p.wakeUp();
/*        System.out.println("firstName: " + p2.firstName);
        System.out.println("lastName: " + p2.lastName);
        System.out.println("age: " + p2.age);*/

//        Student s = new Student("Ваня", "Петькин", 19);
//        s.wakeUp();
//        s.setAge(5);
//        System.out.println(s);
//        p.wakeUp();

//        p.firstName = "new name";
    }

    int countUniqueCombinations(int n, int m) {
        // n = 5 -> n! = 1 * 2 * 3 * 4 * 5
        int a = fact(n);
        int b = fact(n - m);
        b *= fact(m);
        return a / b;
    }

    int fact(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
