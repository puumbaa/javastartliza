package oop.lesson_09;

public class Person {
    protected final String firstName;
    private String lastName;
    private int age;

    public static String bla;


    public int getAge(){
        return age;
    }

    public void setAge(int age){
        if (age >= 0) {
            this.age = age;
        }else{
            System.out.println("данные некорректны!");
        }
    }

    public static int sum(int x, int y) {
        return x + y;
    }

    public void wakeUp(){
        openEyes();
        relax();
        getUp();
        System.out.println("Я проснулся!!");
    }

    private void openEyes(){
        System.out.println("открываю глаза");
    }
    private void relax(){
        System.out.println("валяюсь минут 10 на кровати");
    }
    private void getUp(){
        System.out.println("всё встаю))");
    }



    public Person(String name){
       firstName = name;
    }

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
}
