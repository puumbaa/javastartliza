package oop.lesson_09;

public class Student extends Person{
    int course;

    @Override
    public void wakeUp(){ // переопределение
        System.out.println(firstName + " проснулся и собирается на пары");
    }


    public Student(String firstName, String lastName, int age) {
        super(firstName, lastName, age);
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", age=" + getAge() +
                ", course=" + course +
                '}';
    }
}
