package oop.solid.liskov_substitution;

public class Square extends Rectangle {

    @Override
    public void setWidth(int width) {
        super.width = width;
        height = width;
    }

    @Override
    public void setHeight(int height) {
        width = height;
        super.height = height;
    }

    @Override
    public int getSquare() {
        return height * height; // or width * width
    }
}
