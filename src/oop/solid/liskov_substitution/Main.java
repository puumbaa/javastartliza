package oop.solid.liskov_substitution;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setHeight(5); // a = 5 b = 0
        rectangle.setWidth(3); // a = 5 b = 3
        System.out.println("rectangle.getSquare() = " + rectangle.getSquare());

        Rectangle square = new Square();
        square.setHeight(5); // a = 5 & b = 5
        square.setWidth(3); // a = 3 & b = 3
        System.out.println("square.getSquare() = " + square.getSquare());

    }
}
