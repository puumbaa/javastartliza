package oop.solid.dependency_inversion;

public abstract class Pizza {
    private String name;
    // общие поля
    public abstract void prepare();

    public String getName() {
        return name;
    }
}
