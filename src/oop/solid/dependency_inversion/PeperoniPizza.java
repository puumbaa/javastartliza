package oop.solid.dependency_inversion;

public class PeperoniPizza extends Pizza{
    public void prepare() {
        System.out.println("Нарезаем пепперони...");
        System.out.println("Добавляем остальные ингредиенты...");
        System.out.println("Печем...");
        System.out.println("Пицца с пепперони готова!...");
    }
}
