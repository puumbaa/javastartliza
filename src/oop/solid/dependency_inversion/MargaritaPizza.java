package oop.solid.dependency_inversion;

public class MargaritaPizza extends Pizza{
    public void prepare() {
        System.out.println("Нарезаем помидоры...");
        System.out.println("Добавляем все ингредиенты...");
        System.out.println("Печем...");
        System.out.println("Маргарита готова!");
    }

}
