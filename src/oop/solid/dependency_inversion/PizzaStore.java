package oop.solid.dependency_inversion;


public class PizzaStore {


    public void preparePizza(Pizza pizza){
        System.out.println("Готовим пиццу " + pizza.getName());
        pizza.prepare();
    }

 /*
    плохой код
    public void preparePeperoniPizza(PeperoniPizza pizza){
        System.out.println("готовим пиццу с пепперони...");
        pizza.prepare();
    }

    public void prepareMargaritaPizza(MargaritaPizza pizza){
        System.out.println("готовим маргариту...");
        pizza.prepare();
    }*/

}
