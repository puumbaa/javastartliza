package oop.lesson_12;

public class Child extends Person{
    public Child(String name) {
        super(name);
    }

    @Override
    public void wakeUp() {
        System.out.println(firstName + " проснулся и чиллю");
    }

    public void specificChildMethod(){
        System.out.println("bla bla bla");
    }
}
