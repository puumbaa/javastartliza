package oop.lesson_12;

public class Main {
    public static void main(String[] args) {

        Student student = new Student("Ваня", "Петров", 19);
        Worker worker = new Worker("Работник");
        Child child = new Child("Ребенок");

        Person[] people = {student,worker,child};

        testPerson(student);
        testPerson(worker);

//        testWorker(worker);
//        testStudent(student);

        Person p = new Child("asd");
        p.wakeUp();
//        p.specific


        Child newChild = (Child) p;
        newChild.specificChildMethod();

//        newChild.wakeUp();
//        p.goToWork();


//        for (Person person : people) {
//            person.wakeUp();
//        }
    }
    public static void testPerson(Person person){
        person.wakeUp();
    }
//
//    public static void testWorker(Worker worker){
//        worker.wakeUp();
//    }
//    public static void testStudent(Student student){
//        student.wakeUp();
//    }
}
