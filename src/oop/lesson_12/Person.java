package oop.lesson_12;

public abstract class Person {
    protected  String firstName;
    protected String lastName;
    protected int age;

    public static String bla;


    public int getAge(){
        return age;
    }

    public void setAge(int age){
        if (age >= 0) {
            this.age = age;
        }else{
            System.out.println("данные некорректны!");
        }
    }

    public static int sum(int x, int y) {
        return x + y;
    }

    public void wakeUp(){
        openEyes();
        relax();
        getUp();
        System.out.println("Я проснулся!!");
    }

    private void openEyes(){
        System.out.println("открываю глаза");
    }
    private void relax(){
        System.out.println("валяюсь минут 10 на кровати");
    }
    private void getUp(){
        System.out.println("всё встаю))");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && firstName.equals(person.firstName) && lastName.equals(person.lastName);
    }


    public Person(String name){
        firstName = name;
    }

    public Person(){

    }

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
}
