package oop.lesson_12.hw;

public class Circle extends Shape {

    private double r;
    private String name;

    public double getR(){
        return r;
    }


    public void setR(double r){
        this.r = r;
    }

    public void setName(String name){
        this.name = name;
    }

    public Circle(double r){
        this.r = r;
    }

    @Override
    public double getSquare() {
        return Math.PI * r * r;
    }
}
