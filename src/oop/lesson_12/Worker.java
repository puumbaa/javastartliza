package oop.lesson_12;

public class Worker extends Person{
    public Worker(String name) {
        super(name);
    }

    public Worker(String firstName, String lastName, int age) {
        super(firstName,lastName,age);
    }

    @Override
    public void wakeUp() {
        System.out.println(firstName + " проснулся и собирается на работу");
    }

    public void goToWork(){
        System.out.println("Иду на работу");
    }
}
