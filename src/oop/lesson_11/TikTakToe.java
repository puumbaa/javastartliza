package oop.lesson_11;

import java.util.Scanner;

public class TikTakToe {
    final static char sing_x = 'x';
    final static char sing_y = 'o';
    final static char sing_empty = '.';
    static int count = 0;
    final static char[][] table = new char[3][3];
    static boolean isFirstPlayer = true;


    public static void main(String[] args) {

        System.out.println("Игра началась");
        initTable();
        printTable();

        /*
        *  x x o
        *  o o x
        *  x o x
        * */
        while (true){
            if (makeMove(isFirstPlayer)) {
                break;
            }
           /* if (isFirstPlayer){
                System.out.println("Первый игрок делает ход: ");
                int x = sc.nextInt();
                int y = sc.nextInt();
                if (isCellValid(x,y)){
                    table[x][y] = sing_x;
                    printTable();
                    if (checkWin()) {
                        System.out.println("Выиграл игрок 1");
                        break;
                    }
                    isFirstPlayer = false;
                }
            }else {
                System.out.println("Второй игрок делает ход: ");
                int x = sc.nextInt();
                int y = sc.nextInt();
                if (isCellValid(x,y)){
                    table[x][y] = sing_y;
                    printTable();
                    if (checkWin()) {
                        System.out.println("Выиграл игрок 2");
                        break;
                    }
                    isFirstPlayer = true;
                }
            }*/

/*            if (checkDraw()) {
                System.out.println("Ничья");
                break;
            }*/
        }
    }

    static boolean makeMove(boolean isFirstPlayer){
        Scanner sc = new Scanner(System.in);
        String player = isFirstPlayer ? "Первый" : "Второй";

        System.out.println(player + " игрок делает ход: ");
        int x = sc.nextInt();
        int y = sc.nextInt();
        if (isCellValid(x,y)){
            table[x][y] = isFirstPlayer ? sing_x : sing_y;
            count++;
            printTable();
            if (checkWin()) {
                System.out.println("Выиграл игрок " + player);
                return true;
            }else {
                if (count == 9) {
                    System.out.println("Ничья");
                    return true; // значит что пора прекращать цикл (написать break)
                }
            }
            TikTakToe.isFirstPlayer = !isFirstPlayer;
        }
        return false; // значит что нужно продолжать цикл
    }

    static void initTable() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                table[row][col] = sing_empty;
            }
        }
    }

    static void printTable() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }

    static boolean isCellValid(int x, int y) {
        if (x < 0 || x >= 3 || y < 0 || y >= 3) {
            return false;
        }
        return table[x][y] == sing_empty;
    }


/*    static boolean checkDraw(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == sing_empty) return false;
            }
        }
        return true;
    }*/

    static boolean checkWin(){
        return table[0][0] != sing_empty && table[0][0] == table[0][1] && table[0][1] == table[0][2] ||
                table[1][0] != sing_empty && table[1][0] == table[1][1] && table[1][1] == table[1][2] ||
                table[2][0] != sing_empty && table[2][0] == table[2][1] && table[2][1] == table[2][2] ||
                table[0][0] != sing_empty && table[0][0] == table[1][0] && table[1][0] == table[2][0] ||
                table[0][1] != sing_empty &&  table[0][1] == table[1][1] && table[1][1] == table[2][1] ||
                table[0][2] != sing_empty &&  table[0][2] == table[1][2] && table[1][2] == table[2][2] ||
                table[0][0] != sing_empty &&  table[0][0] == table[1][1] && table[1][1] == table[2][2] ||
                table[0][2] != sing_empty &&  table[0][2] == table[1][1] && table[1][1] == table[2][0];
    }
}