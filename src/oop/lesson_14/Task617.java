package oop.lesson_14;

import java.util.Scanner;

public class Task617 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();

        int cnt = 0;
        for (int i = 5; i > 0; i--) {
            if (x >= i){
                cnt += x/i;
                x %= i;
            }
        }

        System.out.println(cnt);
    }
}
