package oop.lesson_14;
// Солдат и бананы

import java.util.Scanner;

public class Tasks564A {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int k = s.nextInt();
        int n = s.nextInt();
        int w = s.nextInt();


        int x = 0;
        for (int i = 1; i <= w; i++) {
            int y = i * k;
            x = x + y;
        }

        int borrow = x - n;

        System.out.println(Math.max(borrow, 0));
    }
}