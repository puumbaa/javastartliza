package oop.lesson_14;

import java.util.Scanner;

// Мишка и старший брат
public class Task791A {

    public static void main (String[] args) {
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();

        int cnt = 0;
        while (a <= b){
            a *= 3;
            b *= 2;
            cnt++;
        }
        System.out.println(cnt);
    }
}