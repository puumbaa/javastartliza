package oop.lesson_08.hw;

import java.util.Arrays;

public class HW1 {
    public static void main(String[] args) {
        String s = "abc abb abd";
        String[] words = s.split(" ");

        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < words.length - 1; i++) {
                int result = words[i].compareTo(words[i+1]);
                if (result < 0) {
                    isSorted = false;
                    String temp = words[i];
                    words[i] = words[i + 1];
                    words[i + 1] = temp;
                }
            }
        }
        System.out.print(Arrays.toString(words));

    }
}
