package oop.lesson_08.hw;

public class HW2 {
    public static void main(String[] args) {
        String s1 = "Ты люблюбишь есть картошку вечером"; // 34
        String s2 = "любишь есть картошкуу"; // 21

        boolean result = s1.length() > s2.length() ? isSubString(s1, s2) : isSubString(s2, s1);
        System.out.println(result);
    }

    // возвращает сообщение - ответ на вопрос  "является ли 1 строка подстрокой 2-ой строки?"
    public static boolean isSubString(String source, String target) {
        int i = 0;

        while (i < source.length() - target.length() ) {
            while (i < source.length() - target.length() && source.charAt(i) != target.charAt(0)) {
                i++;
            }

            boolean isSubString = true;

            int prevI = i;
            int j = 0;
            while (j < target.length()) {
                if (target.charAt(j) != source.charAt(prevI)) {
                    isSubString = false;
                    break;
                }
                j++;
                prevI++;
            }
            if (isSubString) return true;
            i++;
        }
        return false;
    }
}
