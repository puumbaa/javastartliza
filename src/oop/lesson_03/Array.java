package oop.lesson_03;

import java.util.Scanner;

public class Array {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int size = sc.nextInt();
        int[] array = new int[size];
        System.out.println("Введите значения массива: ");
        // array = {3, 5, 0, 0, 0} //
        for (int i = 0; i < size; i++) {
            array[i] = sc.nextInt();
        }
/*        for (int i = 0; i < size; i++) {
            System.out.println(array[i]);
        }*/

/*      int a = 5;
        int b = 10;
        if (a > b) {
            System.out.println("a больше b");
        }else {
            System.out.println("b больше a");
        }
        */

        int max = Integer.MIN_VALUE; // -2^31
        int min = Integer.MAX_VALUE; // 2^31

        // {2, 3, 8, 5, 1} => {1, 5, 8, 3 ,2}
        // [1, 3] (1,3)
        // [0, size)
        for (int i = 0; i < size; i++) {
            if (array[i] > max) {
                max = array[i];
            }

            if (array[i] < min) {
                min = array[i];
            }
        }
        System.out.println("Максимальный элемент = " + max);

    }
}
