package oop.lesson_03;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        int pr = 1;

        for (int i = 1; i < n; i++) {
            sum += i;
            pr *= i;
        }

        System.out.println("sum = " + sum);
        System.out.println("pr = " + pr);
    }
}
