package oop.lesson_13;

import oop.lesson_13.entities.*;

public class Main  {
    static int i;
    public static void main(String[] args) {
/*      Enum intro
        Scanner sc = new Scanner(System.in);
        String categoryFromScanner = sc.next();
        Category category = Category.valueOf(categoryFromScanner);
        System.out.println(category.getTranslate());*/
        CivilShip civilShip = new CivilShip(50,10);
        Weapon weapon = new Weapon("Blaster", 100);
        CombatShip combatShip = new CombatShip(weapon,100,"Falcon");

        System.out.println("defense before: " + civilShip.getDefense());
        System.out.println("hp before: " + civilShip.getDurability());

        combatShip.atack(civilShip);

        System.out.println("defense after: " + civilShip.getDefense());
        System.out.println("hp after: " + civilShip.getDurability());


        System.out.println(i);
    }
}
