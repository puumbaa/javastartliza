/*
package oop.lesson_13.hw;

import oop.lesson_13.entities.CivilShip;
import oop.lesson_13.entities.CombatShip;
import oop.lesson_13.entities.CommodityShip;
import oop.lesson_13.entities.Ship;

// Ты проектируешь интеллектуальную систему управления ангаром командного центра.
// Реализуй интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используй СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Ship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Ship getMostPowerfulShip(Ship[] ships) {
        int max = -1;
        int maxIndex = -1;

        for (int i = 0; i < ships.length; i++) {

            Ship ship = ships[i];
            if (ship instanceof CombatShip) {
                CombatShip combatShip = (CombatShip) ship;

                // 10 20 20 5 10 20
                if (combatShip.getWeapon().getDamage() > max) {
                    max = combatShip.getWeapon().getDamage();
                    maxIndex = i;
                }
            }
        }
        return maxIndex == -1 ? null : ships[maxIndex];
    }

    @Override
    public Ship getShipByName(Ship[] ships, String name) {
        for (Ship ship : ships) {
            if (ship.getName().equals(name)){
               return ship;
            }
        }
        return null;
    }

    @Override
    public Ship[] getAllShipsWithEnoughCargoSpace(CommodityShip[] ships, int carrying) {

        Ship[] result = new Ship[ships.length];
        int index = 0;
        // [o o o o o] -> [o o o]
        for (CommodityShip ship : ships) {
            if (ship.getCarrying().getBegin() <= carrying || ship.getCarrying().getEnd() >= carrying) {
                result[index] = ship;
                index++;
            }
        }
        return result;
    }

    @Override
    public Ship[] getAllCivilianShips(Ship[] ships) {
        Ship[] result = new Ship[ships.length];
        int index = 0;

        for (Ship ship: ships){
            if (ship instanceof CivilShip){
                result[index] = ship;
                index++;
            }
        }
        return result;
    }
}
*/
