package oop.lesson_13.entities;

import oop.lesson_12.Person;
import oop.lesson_13.entities.interfaces.Defensible;

public class CivilShip extends Ship implements Defensible {
    private int defense;
    private int capacity; // сколько мест впринципе есть у коробля
    private int size; // сколько по факту мест занято сейчас
    private Person[] people;


    @Override
    public int decreaseDefense(int damage) {
        if (defense >= damage){
            defense -= damage;
            damage = 0;
        }else{
            damage -= defense;
            defense = 0;
        }
        return damage;
    }

    public int getDefense() {
        return defense;
    }

    public boolean takeThePlace(Person person) { // [1 3 4 _ _]
        if (capacity > size) { // [p _ _ _ _] size = 1; capacity = 5
            people[size] = person;
            size++;
            return true;
        } else {
            return false;
        }
    }

    public void freeSpace(Person person) {
        for (int i = 0; i < size; i++) { // todo: сделать так чтобы после удаления, элементы хранились друг за другом
            // [3 4 5 _ _]
            if (people[i].equals(person)) {
                people[i] = null;

                for (int j = i; j < capacity - 1; j++) {
                    people[j] = people[j+1];
                }

                if (size == capacity){
                    people[size] = null;
                }
            }
        }
        size--;
    }

    public CivilShip(int defense, int capacity) {
        this.defense = defense;
        this.capacity = capacity;
        this.people = new Person[capacity];
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }
}
