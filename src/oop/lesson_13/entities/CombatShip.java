package oop.lesson_13.entities;

import oop.lesson_13.entities.enums.Category;
import oop.lesson_13.entities.enums.State;
import oop.lesson_13.entities.interfaces.Defensible;

public class CombatShip extends Ship implements Defensible {
    public Weapon getWeapon() {
        return weapon;
    }

    private Weapon weapon;
    private int defense;


    public int decreaseDefense(int damage) {
        damage *= 0.8;
        if (defense >= damage) {
            defense -= damage;
            damage = 0;
        } else {
            damage -= defense;
            defense = 0;
        }
        return damage;
    }

    public void atack(Ship ship) {
        if (weapon != null) {
            int damage = weapon.getDamage();
            /*if (ship instanceof CombatShip || ship instanceof CivilShip) {

                if (ship instanceof CombatShip){
                    CombatShip combatShip =  (CombatShip) ship;
                    if (combatShip.defense >= damage) {
                        combatShip.defense -= damage;
                    }else {
                        damage -= combatShip.defense;
                        combatShip.defense = 0;
                        combatShip.durability -= damage;
                    }
                }else {
                    CivilShip civilShip = (CivilShip) ship;
                    if (civilShip.getDefense() >= damage) {
                        civilShip.setDefense(civilShip.getDefense() - damage);
                    }else {
                        damage -= civilShip.getDefense();
                        civilShip.setDefense(0);
                        civilShip.durability -= damage;
                    }
                }*/
            if (ship instanceof Defensible) {
                Defensible defensibleShip = (Defensible) ship;
                damage = defensibleShip.decreaseDefense(damage);
            }
            ship.durability -= damage;

            if (ship.durability <= 0) {
                ship.state = State.DEAD;
            }
        }
    }

    public CombatShip(Weapon weapon, int defense, String name) {
        super(name, Category.COMBAT);
        this.weapon = weapon;
        this.defense = defense;
    }
}
