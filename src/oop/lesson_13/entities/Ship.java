package oop.lesson_13.entities;

import oop.lesson_13.entities.enums.Category;
import oop.lesson_13.entities.enums.Color;
import oop.lesson_13.entities.enums.State;

public abstract class Ship {


    protected String name;
    protected Category category;
    protected Color color; // todo: переписать с использованием enum
    protected int durability = 100; // hp
    protected State state = State.ALIVE;

    public Ship(){

    }


    public int getDurability() {
        return durability; // Получить поле
    }

    public Ship(String name, Category category) {
        this.name = name;
        this.category = category;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
