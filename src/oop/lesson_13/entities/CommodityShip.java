package oop.lesson_13.entities;

import oop.lesson_13.entities.enums.Carrying;

import oop.lesson_13.entities.interfaces.Defensible;

public class CommodityShip extends Ship implements Defensible{
    // todo: реализовать торговый корабль
    private int defense; // защита
    private Carrying carrying; // грузоподьемность

    public void setDefense (int defense){
        this.defense = defense;
    }

    public void setCarrying(Carrying carrying) {
        this.carrying = carrying;
        if (carrying.equals(Carrying.LARGE)){
            defense+=50;
        }else if (carrying.equals(Carrying.VERY)){
            defense+=80;
        }else{
            defense+=100;
        }
    }


    public Carrying getCarrying() {
        return carrying;
    }

     { // todo: почитать про порядок инициализации объектов в Java.
        System.out.println("bla bla");
    }

    @Override
    public int decreaseDefense(int damage) {
        damage *= 2;
        if (defense >= damage){
            defense -= damage;
            damage = 0;
        }else{
            damage -= defense;
            defense = 0;
        }
        return damage;
    }



}
