package oop.lesson_13.entities.enums;

public enum Category {
    COMBAT("боевой"),
    COMMODITY("товарный"),
    CIVIL("гражданский");

    private final String translate;


    public String getTranslate() {
        return translate;
    }

    Category(String translate) {
        this.translate = translate;
    }

}
