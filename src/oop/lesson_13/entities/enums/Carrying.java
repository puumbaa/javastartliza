package oop.lesson_13.entities.enums;

public enum Carrying {
    ULTRA(300,500), VERY(180,299), LARGE(80,179);

    private int begin;
    private int end;

    public int getBegin() {
        return begin;
    }

    public int getEnd() {
        return end;
    }

    Carrying(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }


}
