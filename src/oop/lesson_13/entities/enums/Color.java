package oop.lesson_13.entities.enums;

public enum Color {

    PINK("Розовый"),
    GREEN("Зеленый"),
    PURPLE("Фиолетовый");

    private String translate;

    public String getTranslate() {
        return translate;
    }

    Color(String translate) {
        this.translate = translate;
    }
}