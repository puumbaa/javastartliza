package oop.lesson_04;

import java.util.Arrays;
import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 5;
        int[] a = {2, 1, 2, 4, 3}; // {1, 2, 2, 3, 4}
        /*
         * 1 счетчик (глобальный максимум) - текущее максимальное количество ("Какое на данный момент максимально количество")
         * 2 счетчик (локальный) - количество текущего элемента - отвечает на вопрос
         *  "Сколько раз встретился текущий элемент? "*/

        // [1 2 2 3 4]
        System.out.println("до " + Arrays.toString(a));

        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i + 1]) {
                    isSorted = false;
                    int temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                }
            }
        }
        System.out.println("после " + Arrays.toString(a));

        // todo найти ошибку в логике работы алгоритма
        int maxCount = -1; // 2
        for (int i = 0; i < a.length - 1; i++){
            int currentCount = 1;
            if (currentCount > maxCount){
                maxCount = currentCount;
            }
        }
        System.out.println(maxCount);

//        int[] arr = {3,5}; // 5 3
//        System.out.println("до: " + Arrays.toString(arr));
//        int temp = arr[0]; // 3
//        arr[0] = arr[1]; // 5 5
//        arr[1] = temp; // 5 5
//        System.out.println("после: " + Arrays.toString(arr));

    }
}
