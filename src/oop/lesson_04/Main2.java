package oop.lesson_04;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 5;
        int[] a = {2, 1, 2, 4, 3}; // {1, 2, 2, 3, 4}
        int max = a[0];
        for (int i = 0; i < n; i++) {
            if (a[i] > max) {
                max = a[i];
            }
        }
        int[] d = new int[max + 1];

        for (int i = 0; i < n; i++) {
//            int index = a[i];
//            d[index]++;
            d[a[i]]++;
        }
        int maxCnt = d[0];
        for (int i = 0; i < d.length; i++) {
            if (d[i] > maxCnt) {
                maxCnt = d[i];
            }
        }
        System.out.println(d);
    }
}
