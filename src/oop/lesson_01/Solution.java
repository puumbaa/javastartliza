package oop.lesson_01;

public class Solution {
    public static void main(String[] args) {
        int myIntVariable = 12;
        double myDoubleVariable = 12.5;
        boolean myBooleanVariable = 12 != 10 && 12 > 0 || 12 < 5;
        String str = "hello";
        String str1 = new String("hello");

//        System.out.print(myBooleanVariable);

        int resultOp1 = 13 / 2;
        int resultOp2 = 14 % 2;

        boolean x = myIntVariable == 10;
        long myLongVariable =  myIntVariable;

        myIntVariable *= 2;
        System.out.println(myIntVariable);

    }
}
