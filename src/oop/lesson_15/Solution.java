package oop.lesson_15;

public class Solution {
    public static void main(String[] args) {
        // раннее связывание / позднее связывание / статический и динамический полиморфизм
        // print(); - перегрузка

/*        Parent parent = new Child1();
        parent.test();

//        print(); - > System.out.println("printing message: " + message + "...");
        print("Hello");*/
        Child2 child2 = new Child2();
        child2.staticMethod();


    }

    public static void print(){
        System.out.println("printing messsage....");
    }

    public static void print(String message){
             System.out.println("printing message: " + message + "...");
    }

    public static void print(String message, int x){
        System.out.println("printing message: " + message + "...");
    }



}
