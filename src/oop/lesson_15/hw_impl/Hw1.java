package oop.lesson_15.hw_impl;

import java.util.ArrayList;

public class Hw1 {
    public static void main(String[] args) {
        ArrayList<Integer> source = new ArrayList<>();
        source.add(9);
        source.add(8);
        source.add(7);
        source.add(12);
        source.add(18);
        source.add(1);

        int maxCount = 0;

        for (Integer i : source) {
            if (i > maxCount) {
                maxCount = i;
            }
            if (i < maxCount){
                break;
            }
            System.out.println(i);
        }

    }
}
