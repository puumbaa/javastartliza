package oop.lesson_15.post_office;

public class PigeonMail extends PostOffice {
    private String species; // порода
    private int speed; // скорость

    public void fly() { // летит

    }

    public void lendOn() { // приземляется

    }

    @Override
    public void sendMailOrder(MailOrder order) {
        if (order instanceof Letters) {
            order.send();
        } else {
            System.out.println("Посылка не является письмом");
        }
    }

    @Override
    public void confirmMailOrder(MailOrder order) {
        if (order instanceof Letters) {
            order.confirm();
        } else {
            System.out.println("Посылка не является письмом");
        }
    }

}
