package oop.lesson_15.post_office;

public enum Status {
    PROCESSING, DELIVERY, RECEIVED;
}
