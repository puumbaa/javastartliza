package oop.lesson_15.post_office;

import java.util.Arrays;

// todo: создать как минимум две реализации MailOrder

public abstract class MailOrder implements Sendable, Confirmable {
    protected int weight;
    protected String from; // откуда
    protected String to; // куда
    protected Feature[] feature;
    protected Status status;

    protected MailOrder(int weight, String from, String to, Feature[] feature, Status status) {
        this.weight = weight;
        this.from = from;
        this.to = to;
        this.feature = feature;
        this.status = status;

    }

    @Override
    public String toString() {
        return "MailOrder{" +
                "weight=" + weight +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", feature=" + Arrays.toString(feature) +
                ", status=" + status +
                '}';
    }
}
