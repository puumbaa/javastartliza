package oop.lesson_15.post_office;

public interface Sendable {
    void send();
}
