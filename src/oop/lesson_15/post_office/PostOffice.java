package oop.lesson_15.post_office;

// Родительский класс
// TODO: СДЕЛАТЬ ЕЩЕ ОДНУ РЕАЛИЗАЦИЮ ПОЧТЫ
public abstract class PostOffice {
    protected int staffCapacity;
    protected String name;
    protected String address;
    protected Size size;

    public abstract void sendMailOrder(MailOrder order);
    public abstract void confirmMailOrder(MailOrder order);
}