package oop.lesson_15.post_office;


public class Employee {
    private String name;
    private String lastName;
    private int exp;
    private PostOffice office;

    public Employee(String name, String lastName, int exp) {
        this.name = name;
        this.lastName = lastName;
        this.exp = exp; // ОПЫТ
    }

    public Employee(String name, String lastName, int exp, PostOffice office) {
        this(name, lastName, exp);
        this.office = office;
    }
}
