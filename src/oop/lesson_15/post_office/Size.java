package oop.lesson_15.post_office;

public enum Size {
    LOW(100), MEDIUM(250), LARGE(500);
    private int value;

    Size(int value) {
        this.value = value;
    }
}
