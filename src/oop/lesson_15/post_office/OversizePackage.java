package oop.lesson_15.post_office;
// крупногабаритная посылка


public class OversizePackage extends MailOrder{

    public OversizePackage(int weight, String from, String to, Feature[] feature, Status status) {
        super(weight, from, to, feature, status);
    }

    @Override
    public void confirm() {

    }

    @Override
    public void send() {

    }
}
