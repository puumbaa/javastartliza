package oop.lesson_15.post_office;

public class Main {
    public static void main(String[] args) throws Exception{
        MailOrder[] orders = new MailOrder[3];

        orders[0] = new Letters(12,Country.RUSSIA.name(),Country.GEORGIA.name(), null, Status.PROCESSING);
        orders[1] = new OversizePackage(120,Country.RUSSIA.name(),Country.GEORGIA.name(), null, Status.PROCESSING);
        orders[2] = new ColoredLetters(12,Country.RUSSIA.name(),Country.GEORGIA.name(), null, Status.PROCESSING, "red");


        PostOffice office = new PigeonMail();


        for(MailOrder order: orders){
            office.sendMailOrder(order);
            Thread.sleep(2000);
            office.confirmMailOrder(order);
        }

    }
}
