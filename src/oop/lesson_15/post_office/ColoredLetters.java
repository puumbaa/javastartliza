package oop.lesson_15.post_office;

public class ColoredLetters extends Letters{

    public ColoredLetters(int weight, String from, String to, Feature[] feature, Status status, String color) {
        super(weight, from, to, feature, status);
        this.color = color;
    }

    private String color;
}
