package oop.lesson_15.post_office;
// письмо

import java.util.Arrays;

public class Letters extends MailOrder{

    public Letters(int weight, String from, String to, Feature[] feature, Status status) {
        super(weight, from, to, feature, status);
    }


    @Override
    public String toString() {
        return "Letters{" +
                "weight=" + weight +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", feature=" + Arrays.toString(feature) +
                ", status=" + status + '}';
    }

    @Override
    public void confirm() {
        if (this.status.equals(Status.DELIVERY)){
            System.out.println("В " + to + " подтвердили получение");
            status = Status.RECEIVED;
            System.out.println(this);
        }
    }

    @Override
    public void send() {
        System.out.println("Отправлено в " + to);
        status = Status.DELIVERY;
        System.out.println(this);
    }
}
