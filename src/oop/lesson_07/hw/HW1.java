package oop.lesson_07.hw;

import java.util.Scanner;

public class HW1 {
    public static void main(String[] args) {
        Scanner sk = new Scanner(System.in);
        System.out.println("Введите слово ");
        String s = sk.nextLine();
/*        s = s.replace(" ", "");
        char[] ch = s.toCharArray();
        StringBuilder text = new StringBuilder(); // String text = "";
        for (int i = ch.length - 1; i >= 0; i--) {
            text.append(ch[i]);
        }
        if (s.equalsIgnoreCase(text.toString())) {
            System.out.println("Слово является палиндромом");
        }
        else {
            System.out.println("Слово не является палиндромом");
        }*/
        // abccba
        boolean isPalindrom = true;
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                isPalindrom = false;
                break;
            }
        }
        System.out.println(isPalindrom ?  "палиндром"  : "не палиндром");
    }
}
