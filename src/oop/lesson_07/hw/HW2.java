package oop.lesson_07.hw;

public class HW2 {
    public static void main(String[] args) {
        String s = "xzhjo";  // i = 5 | j = 6
        boolean isGoodString = true; // если false - дубликаты есть, true - их нет

        for (int i = 0; i < s.length(); i++) {
            for (int j = s.length() - 1; j > i; j--) {
                if (s.charAt(i) == s.charAt(j)) {
                    isGoodString = false;
                    break;
                }
            }
            if (!isGoodString) {
                break;
            }
        }
        // условие ? (если true) : (если false)
        System.out.println(isGoodString ? "дубликатов нет" : "дубликаты есть");

/*        if (isGoodString){
            System.out.println("дубликатов нет");
        }else {
            System.out.println("дубликаты есть");
        }*/

/*        for (int i = 0; i < s.length() - 1; i++) {
            for (int j = i+1; j < s.length(); j++) {
                if (s.charAt(i) == s.charAt(j)) {
                    System.out.println("В строке не все символы уникальны");
                } else {
                    System.out.println("В строке все символы уникальны");
                }
            }
        }*/
    }
}