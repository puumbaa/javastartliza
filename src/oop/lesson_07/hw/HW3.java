package oop.lesson_07.hw;

public class HW3 {
    public static void main(String[] args) {
        int[] arr = {1,2,3,6,5,4,3}; // 1 > 2 > 3 < 4,3,2,5}
        boolean flag = false;
        for (int i = 0; i < arr.length - 3; i++) {
            if (arr[i] > arr[i+1] && arr[i+1] > arr[i+2] && arr[i+2] <= arr[i+3]) {
                flag = true;
                break;
            }
        }
        System.out.println(flag ? "все отлично" : "всё плохо");
    }
}
