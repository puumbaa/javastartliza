package oop.lesson_07.hw;

import java.util.Arrays;
import java.util.Scanner;

public class HW4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int size = sc.nextInt();
        int[] array = new int[size];
        System.out.println("Введите значения массива: ");
        for (int i = 0; i < size; i++) {
            array[i] = sc.nextInt();
        }
        int[] array2 = new int[size];

        for (int i = 0; i < size; i++) {
            array2[i] = array[size - i - 1];
        }

        System.out.println (Arrays.toString(array2));
    }
}
