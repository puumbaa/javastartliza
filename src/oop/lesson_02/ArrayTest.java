package oop.lesson_02;

import java.util.Scanner;

public class ArrayTest {
    public static void main(String[] args) {
/*        int[] a = {10, 11, 12, 13, 14};
        int[] b = {15, 16, 17};

        int[] ab = new int[a.length + b.length];*/

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите кол-во элементов массива: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];

        System.out.println("Введите значения массива: ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.println("Введенный массив: ");
        for (int i = 0; i < size; i++) {
            System.out.println(arr[i]);
        }

    }
}
