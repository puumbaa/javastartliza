package oop.lesson_02;

public class Solution {
    public static void main(String[] args) {
/*        Scanner myScanner = new Scanner(System.in);
        String name = myScanner.nextLine();

        if (name.equals("Liza")) {
            System.out.println(1);
        } else if (name.equals("Elnur")) {
            System.out.println(-1);
        } else {
            System.out.println(0);
        }
        */

        // обзор цикла while
        int count = 0;
//        System.out.println("до цикла count = " + count);
        while (count < 5){
            count++;
        }
//        System.out.println("после цикла count = " + count);

        for (int i = 0; i < 10; i++) {
//            System.out.println("i = " + i);
        }

        // count = 5
        do {
            count++;
            if (count == 8) {
                continue;
            }
            System.out.println("count = " + count);
        }while (count < 10);

//        System.out.println("count = " + count);
    }
}
