package oop.lesson_06;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int[] arr2 = new int[arr.length]; // arr2 = { 5 0 0 0 0 }

        for (int i = 0; i < arr.length; i++) { // i = 0,1,2,3,4 | arr.length = 5
            arr2[i] = arr[arr.length - i - 1];
             /*todo проблемы такого решения:
             *  1. на 0-ой итерации будем брать arr[5] а такого нет
             *  2. на последней 4-ой итерации получаем элемент arr[1]. т.е. никак не обрабатываем arr[0]*/
        }
        System.out.println(Arrays.toString(arr2));

/*
        for (int x: arr){ // только чтение массива
            System.out.print(x + " ");
        }

        for (int i = 0; i < arr.length; i++) { // чтение + запись в массив
            System.out.println(arr[i] + " ");
            arr[i]++;
        }
*/

    }
}
