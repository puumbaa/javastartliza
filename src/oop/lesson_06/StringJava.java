package oop.lesson_06;

import java.util.Scanner;

public class StringJava {
    public static void main(String[] args) {
        int x = 5; // Л - 16 ->  |00000000|00001000|
        String str1 = "Лиза"; // char[] value =  {'Л', 'и', 'з', 'a'};
        str1 = str1.toUpperCase();
//        System.out.println(str);

        char[] arr = {'h','e','l','l','o'};
        String str2 = new String(); // аналогично String str2 = "";
//        System.out.println(str2);

 /*       String a = new String("abc");
        String b = new String("abc");


        if (a.equals(b)) {
            System.out.println("a = b");
        }else{
            System.out.println("a != b");
        }*/

/*        String a = "abc";
        String b = "abc";
        if (a == b) {
            System.out.println("a = b");
        }*/

        String a = "Hello"; // HELLO
        String b = "hello"; // HELLO
/*        System.out.println(a.equals(b));
        System.out.println(a.equalsIgnoreCase(b));
        System.out.println(a.charAt(1)); // e
        char[] charArray = b.toCharArray(); // [h,e,l,l,o]
        System.out.println(a.concat(b)); // Hellohello
        System.out.println(a.toUpperCase());
        System.out.println(a.toLowerCase());*/

        String abc = "abc";
        String abd = "abd";
//        System.out.println(abc.compareTo(abd));

        /* a.compareTo(b) выведет 0 если a = b,
        выведет значение > 0, если а > b ,
        выведет значение < 0, если a < b
        */
//        System.out.println(abc.compareToIgnoreCase(abd));

        // abccba
        String str = "hello how are you?";
        String[] strArray = str.split(" "); // [hello, how, are, you?]
//        System.out.println(Arrays.toString(strArray));
        System.out.println("index of 'o' = " + str.indexOf("o"));
        System.out.println("last index of 'o' = " + str.lastIndexOf("o"));
        System.out.println(str.substring(str.lastIndexOf('h'),str.lastIndexOf('e') + 1));
        System.out.println(str.substring(str.lastIndexOf('h')));

        // Чтение с клавиатуры
        Scanner sc = new Scanner(System.in);
//        String s = sc.next();  hello how are you ?  \ считается hello
        String s = sc.nextLine(); // hello how are you ?  \ считается hello how are you ?
    }
}
